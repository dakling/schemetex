(define-module (schemetex)
  #:use-module (scheme file)
  #:export (concat-space
            tex-eval
            tex-eval-list
            extract-list-of-strings
            tex-to-string
            tex-cmd
            tex-inverse-cmd
            tex-env
            tex-newline
            tex-math
            tex-eq-newline
            wrap-parens
            wrap-parens-short
            wrap-braces
            wrap-braces-short
            wrap-brackets
            wrap-brackets-short
            make-exponent
            make-index
            repeat-string
            write-tex-file))

(define* (concat-space #:rest input)
  (if (and input (not (null? input)) (car input))
      (string-append
       (tex-to-string (car input))
       " "
       (apply concat-space (cdr input)))
    ""))


(define (tex-to-string input)
  (cond
   ((null? input) "")
   ((not input) "")
   ((unspecified? input) "")
   ((list? input) (string-append (tex-to-string (car input)) (tex-to-string (cdr input))))
   ((string? input) input)
   ((number? input) (number->string input))
   (#t (symbol->string input))))

(define (tex-eval content)
  (cond
   ;;"base cases"
   ;; ((null? content) "")
   ;; ((not content) "")
   ((number? content) content)
   ((not (pair? content)) (tex-to-string content))
   ;;"special forms"
   ((and (list? content) (eq? (car content) 'list))
    (apply list
           (map tex-eval
                (cdr content))))
   ((and (list? content) (eq? (car content) 'group))
    (apply concat-space
           (map tex-eval
                (cdr content))))
   ;;"handle nested lists"
   ((list? content)
    (apply
     (primitive-eval (car content))
     (tex-expand (cdr content))))))

(define (tex-expand content-list)
  (if (or (null? content-list) (not content-list))
      '()
      (cons (tex-eval (car content-list))
            (tex-expand (cdr content-list)))))

(define (tex-eval-list content)
  (if (list? (car content))
      (extract-list-of-strings (map tex-eval content))
      (tex-eval content)))

(define (list-if-not-already input)
  "writes input into a lisp unless it is already a list"
  (if (list? input)
      input
      (list input)))

(define* (extract-list-of-strings string-list #:optional pre post)
  "extract a list of strings and frame each item by pre and post"
  (cond
   ((null? string-list) "")
   ((not string-list) "")
   ((unspecified? string-list) "")
   (#t (string-append
        (tex-to-string pre)
        (tex-to-string (if (pair? string-list) (car string-list) string-list))
        (tex-to-string post)
        (extract-list-of-strings (when (pair? string-list) (cdr string-list)) pre post)))))

(define (repeat-string string count)
  (when (> count 0)
    (concat-space
     (tex-to-string string)
     (repeat-string string (- count 1)))))

(define* (tex-cmd cmd-name #:optional mandatory-args optional-args triangle-args)
  "create a tex command accepting single arguments or multiple arguments as a list"
  (let
      ((mandatory-arg-list
        (list-if-not-already mandatory-args))
       (optional-arg-list
        (list-if-not-already optional-args))
       (triangle-arg-list
        (list-if-not-already triangle-args)))
    (string-append
     "\\"
     (tex-to-string cmd-name)
     (if triangle-args
         (extract-list-of-strings triangle-arg-list "<" ">")
         "")
     (if optional-args
         (extract-list-of-strings optional-arg-list "[" "]")
         "")
     (if mandatory-args
         (extract-list-of-strings mandatory-arg-list "{" "}")
         ""))))

(define* (tex-inverse-cmd cmd-name #:optional mandatory-args optional-args)
  "create a tex command accepting single arguments or multiple arguments as a list"
  (let ((mandatory-arg-list
         (list-if-not-already mandatory-args))
        (optional-arg-list
         (list-if-not-already optional-args)))
    (string-append
     "\\"
     (tex-to-string cmd-name)
     (if mandatory-args
         (extract-list-of-strings mandatory-arg-list "{" "}")
         "")
     (if optional-args
         (extract-list-of-strings optional-arg-list "[" "]")
         ""))))

(define (wrap inner deliml delimr)
  (string-append (tex-to-string deliml)
                 (tex-to-string inner)
                 (tex-to-string delimr)))

(define* (wrap-parens #:rest inner)
  (wrap (concat-space inner)
        (tex-cmd "left(")
        (tex-cmd "right)")))

(define* (wrap-parens-short #:rest inner)
  (wrap (concat-space inner)
        "("
        ")"))

(define* (wrap-braces #:rest inner)
  (wrap (concat-space inner)
        "{"
        "}"))

(define* (wrap-brackets #:rest inner)
  (wrap (concat-space inner)
        (tex-cmd "left[")
        (tex-cmd "right]")))

(define* (wrap-brackets-short #:rest inner)
  (wrap (concat-space inner)
        "["
        "]"))

(define (write-tex-file filename content)
  "write content to file"
  (call-with-output-file
      filename
    (lambda (port)
      (display content port))))

(define (make-exponent main-expression exponent)
  (string-append (tex-to-string main-expression) "^" (wrap-braces (tex-to-string exponent))))

(define* (make-index main-expression #:rest indices)
  (string-append (tex-to-string main-expression) "_" (wrap-braces (extract-list-of-strings indices))))

(define (tex-newline)
  (string #\Newline))

(define (tex-eq-newline)
  (string-append "\\\\" (tex-newline)))

(define* (tex-env env-name content #:optional mandatory-args optional-args pre-cmd post-cmd)
  "create a tex environment accepting single arguments or multiple arguments as a list"
  (let ((mandatory-arg-list
         (list-if-not-already mandatory-args))
        (optional-arg-list
         (list-if-not-already optional-args)))
    (string-append (tex-newline)
                   (tex-inverse-cmd
                    "begin"
                    (cons env-name (if mandatory-args
                                       mandatory-arg-list
                                       '()))
                    (if optional-args optional-arg-list #f))
                   (if (not (or (null? pre-cmd) (not pre-cmd)))
                       (string-append
                        (tex-newline)
                        pre-cmd)
                       "")
                   (tex-newline)
                   (tex-to-string content)
                   (if post-cmd post-cmd "")
                   (tex-newline)
                   (tex-cmd "end" (list env-name)))))

(define (tex-symbol symbol-name)
  (string-append " " (tex-cmd symbol-name) " "))

(define* (tex-math #:rest content)
  (wrap (concat-space content) (tex-cmd "(") (tex-cmd ")")))
