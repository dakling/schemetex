(add-to-load-path (dirname "../../"))

(use-modules
 (schemetex)
 (schemetex stdlib)
 (schemetex math))

;;; this example shows how to generate an entire document from lisp
(write-tex-file
 "example-scheme.tex"
 (tex-eval-list
  '((tex-documentclass article)
    ;; (tex-usepackages ((amsmath)))
    (tex-usepackage amsmath)
    (tex-document
     (group
      (tex-section
       (group
	Create a latex document from lisp)
       (group
	In this document we show how to create an entire latex document from lisp.
	It is recommended to use org-mode instead.))
      (tex-section "Using strings"
       (group
	    "Strings can be used if you prefer, and if you want to use Parentheses and
stuff. For longer sequences it certainly makes sense. Perhaps it is more
convenient not to use tex-eval-list on the entire document, but only on the
important parts.
Since I don't really use this workflow personally, I am not sure :)"
        (tex-eq 1+1=2)
        (tex-multi-eq
         (list
          (group
           1 + 1 &= 2)
          (group
           (dd y x 2) + y &= 0))
         (list
          eq-simple
          eq-differential)))))))))

;; (display
;;  (tex-eval-list
;;   '((tex-documentclass article)
;;     ;; (tex-usepackages ((amsmath)))
;;     (tex-usepackage amsmath)
;;     (tex-document
;;      (group
;;       (tex-section
;;        (group
;; 	    Create a latex document from lisp)
;;        (group
;; 	    In this document we show how to create an entire latex document from lisp.
;; 	    It is recommended to use org-mode instead.))
;;       (tex-section "Using strings"
;;                    (group
;; 	                "Strings can be used if you prefer, and if you want to use Parentheses and
;; stuff. For longer sequences it certainly makes sense. Perhaps it is more
;; convenient not to use tex-eval-list on the entire document, but only on the
;; important parts.
;; Since I don't really use this workflow personally, I am not sure :)"
;;                     (tex-eq 1+1=2)
;;                     (tex-multi-eq
;;                      (list
;;                       (group
;;                        1 + 1 &= 2)
;;                       (group
;;                        (dd y x 2) + y &= 0))
;;                      (list
;;                       eq-simple
;;                       eq-differential)))))))))
