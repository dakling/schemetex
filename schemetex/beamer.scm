(define-module (schemetex beamer)
  #:use-module (srfi srfi-1)
  #:use-module (schemetex)
  #:use-module (schemetex stdlib)
  #:export (beamer-frame
            beamer-set-template
            tex-eq-pause
            onslide
            only))

(define (beamer-frame title content)
  (tex-env "frame" content title #f (tex-newline) (tex-newline)))

(define (beamer-set-template item style)
  (tex-inverse-cmd "setbeamertemplate" item style))

(define* (extract-single-equation-list-pause eq-list #:optional omit-newlines-p label)
  "extract a single multiline equation, with lines given as a list"
  (string-append (extract-list-of-strings
                  (drop-right eq-list 1)
                  (string-append "\\onslide<+->{" (tex-newline))
                  (string-append "}" (if omit-newlines-p "" (tex-eq-newline))))
                 (string-append "\\onslide<+->{" (tex-newline))
                 (last eq-list)
                 "}"))

(define* (tex-eq-pause body #:optional label punctuation)
  "create a single latex equation, possibly spanning multiple lines"
  (cond ((list? body)
         (tex-env "align*"
                  (string-append (extract-single-equation-list-pause body label))))
        (#t
         (tex-env "equation*"
                  body))))

(define* (generic-overlay name content #:optional count-from count-to)
  (tex-cmd name
           content
           #f
           (cond
            ((and count-from count-to)
             (string-append
              (tex-to-string count-from)
              "-"
              (tex-to-string count-to)))
            (count-from
             (string-append
              (tex-to-string count-from)
              "-"))
            (count-to
             (string-append
              "-"
              (tex-to-string count-to)))
            (#t "+-"))))

(define* (onslide content #:optional from to)
  (generic-overlay "onslide" content from to))

(define* (only content #:optional from to)
  (generic-overlay "only" content from to))
