(define-module (schemetex math)
  #:use-module (srfi srfi-1)
  #:use-module (schemetex)
  #:use-module (schemetex stdlib)
  #:export (tex-eq
            tex-multi-eq
            frac
            diff
            pd
            dd
            kronecker-delta
            permutation-epsilon
            equal-?
            equal-!
            lsin
            lcos
            ltan
            normal-vector
            power
            square))

(define* (extract-single-equation-list eq-list #:optional label)
  "extract a single multiline equation, with lines given as a list"
  (string-append (extract-list-of-strings
                  (drop-right eq-list 1)
                  (string-append (tex-cmd "nonumber") (tex-newline))
                  (tex-eq-newline))
                 (if (not (null? label)) (tex-label label) (tex-cmd "nonumber"))
                 (tex-newline)
                 (last eq-list)))

(tex-eq-newline)

(define* (tex-eq body #:optional label punctuation)
  "create a single latex equation, possibly spanning multiple lines"
  (cond ((list? body) (tex-env "align"
                               (string-append (extract-single-equation-list body label)
                                              #f #f #f (if (null? punctuation) "," punctuation))))
        (#t (tex-env "equation"
                     body
                     #f
                     #f
                     (if label (tex-label label) (tex-cmd 'nonumber))
                     (if (null? punctuation) "," punctuation)))))

(define* (extract-multi-equation-list eq-list #:optional label-list)
  "extract multiline equations, with equations given as a list"
  (string-append
   (extract-list-of-strings
    (map (lambda* (eq #:optional label)
           (extract-list-of-strings (list eq) (if label (concat-space (tex-label label) (tex-newline)) #f) (tex-eq-newline)))
         (drop-right eq-list 1)
         (if (or (not label-list) (null? label-list)) (make-list (1- (length eq-list)) #f) (drop-right label-list 1))))
   (extract-list-of-strings (last eq-list) (when label-list (concat-space (tex-label (last label-list)) (tex-newline))) (tex-newline))))

(define* (tex-multi-eq eq-list #:optional label-list punctuation)
  "write multiple equations, each one possibly spanning multiple lines"
  (tex-env "align"
           (string-append
            (extract-multi-equation-list eq-list label-list)
            (if punctuation punctuation ","))))

(define (frac nom denom)
  (tex-cmd 'frac (list nom denom)))

(define* (diff y x #:optional diff-symbol (order 1))
  (cond ((equal? "dot" (tex-to-string diff-symbol))
         (if (and order (> order 1))
             (tex-cmd 'dot y)
             (error "Higher orders not implemented with dots")))
        ((equal? "prime" (tex-to-string diff-symbol))
         (string-append (make-exponent
                         y
                         (repeat-string (tex-cmd 'prime) order))))
        (#t
         (if (or (not order) (= order 1))
             (frac (string-append (tex-to-string diff-symbol) " " (tex-to-string y))
                   (string-append (tex-to-string diff-symbol) " " (tex-to-string x)))
             (frac (string-append (make-exponent diff-symbol order) " " (tex-to-string y))
                   (make-exponent (string-append (tex-to-string diff-symbol) " " (tex-to-string x)) order))))))

(define* (dd y x #:optional order)
  (diff y x 'd order))

(define* (pd y x #:optional order)
  (diff y x (tex-cmd 'partial) order))

(define (kronecker-delta i j)
  (make-index (tex-cmd 'delta) i j))

(define (permutation-epsilon i j k)
  (make-index (tex-cmd 'epsilon) i j k))

(define (equal-?) (tex-cmd 'stackrel (list "?" "=")))

(define (equal-!) (tex-cmd 'stackrel (list "!" "=")))

(define* (lcos #:rest args)
  (tex-cmd 'cos args))

(define* (lsin #:rest args)
  (tex-cmd 'sin args))

(define* (ltan #:rest args)
  (tex-cmd 'tan args))

(define (normal-vector index)
  (make-index "n" index))

(define (power base exponent)
  (make-exponent base exponent))

(define* (square #:rest base)
  (power (extract-list-of-strings base) 2))
