(define-module (schemetex stdlib)
  #:use-module (schemetex)
  #:export (tex-documentclass
            tex-document
            tex-usepackage
            tex-usepackages
            tex-chapter
            tex-section
            tex-subsection
            tex-subsubsection
            tex-itemize
            tex-figure
            tex-label))

;; header
(define* (tex-documentclass class #:rest options)
  (string-append (tex-cmd 'documentclass class options)
	  (tex-newline)))

(define* (tex-usepackage package #:rest options)
  (string-append
   (tex-cmd 'usepackage package options)
   (tex-newline)))

(define (tex-usepackages package-list)
  (unless (null? package-list)
    (string-append
     (cond
      ((list? (car package-list)) (tex-usepackage (caar package-list) (cadar package-list)))
      (#t (tex-usepackage (car package-list))))
     (tex-usepackages (cdr package-list)))))

(define (tex-input-file file)
  (string-append (tex-cmd 'input file)
	  (tex-newline)))

(define (tex-document content)
  (string-append    (tex-newline)
   (tex-env 'document content)))
;; sections
(define (generic-tex-structure structure-type title content)
  (string-append
   (tex-newline)
   (tex-cmd structure-type title)
   (tex-newline)
   (tex-newline)
   content
   (tex-newline)))

(define (tex-chapter title content)
  (generic-tex-structure 'chapter title content))

(define (tex-section title content)
  (generic-tex-structure 'section title content))

(define (tex-subsection title content)
  (generic-tex-structure 'subsection title content))

(define (tex-subsubsection title content)
  (generic-tex-structure 'subsubsection title content))

;; environments
(define* (tex-itemize item-list #:optional one-at-a-time-p)
  (tex-env 'itemize
	   (extract-list-of-strings item-list (string-append (tex-cmd 'item) " ") (tex-newline))
	   #f
	   (when one-at-a-time-p "<+->")))

(define* (tex-figure filename #:optional width caption label)
  (tex-env
   'figure
   (string-append (tex-cmd 'includegraphics filename (string-append "width=" (tex-to-string (if width width 1)) (tex-cmd 'textwidth)))
    (when label (tex-cmd 'label label))
    (when caption (tex-cmd 'caption* caption)))))

;; references, citations
(define (tex-label label)
  (unless (null? label) (string-append (tex-cmd "label" label))))
