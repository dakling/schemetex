(define-module (schemetex tikz)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-9)
  #:use-module (schemetex)
  #:use-module (schemetex stdlib)
  #:use-module (schemetex beamer)
  #:export
  (tikz-picture
   make-point
   make-node
   make-path-annotation
   make-path
   draw-node
   draw-path-annotation
   draw-path))

(define-record-type point
  (make-point x y)
  point?
  (x point-x set-point-x!)
  (y point-y set-point-y!))

(define-record-type node
  (make-node-primitive
   content
   label
   location
   other-options
   style
   color
   shape
   text-width
   text-align
   on-slides)
  node?
  (content node-content)
  (label node-label)
  (location node-location set-node-location!)
  (other-options node-other-options)
  (style node-style)
  (color node-color)
  (shape node-shape)
  (text-width node-text-width set-node-text-width!)
  (text-align node-text-align)
  (on-slides node-on-slides set-node-on-slides!))

(define* (make-node
          content
          label
          #:optional
          (location (make-point 0 0))
          other-options
          (style "draw, rounded corners")
          (color "black!20")
          (shape "rectangle")
          (text-width "20em")
          (text-align "text centered")
          on-slides)
  (make-node-primitive
   content
   label
   location
   other-options
   style
   color
   shape
   text-width
   text-align
   on-slides))

(define-record-type path-annotation
  (make-path-annotation-primitive
   content
   label
   other-options
   shape
   color
   text-align
   text-width
   position
   on-slides)
  path-annotation?
  (content path-annotation-content)
  (label path-annotation-label)
  (other-options path-annotation-other-options)
  (color path-annotation-color)
  (shape path-annotation-shape)
  (text-align path-annotation-text-align)
  (text-width path-annotation-text-width set-path-annotation-text-width!)
  (position path-annotation-position set-path-annotation-position!)
  (on-slides path-annotation-on-slides set-path-annotation-on-slides!))

(define* (make-path-annotation
          content
          label
          #:optional
          other-options
          (shape "ellipse")
          (color "black!20")
          (text-align "text centered")
          (text-width "10em")
          (position "above")
          on-slides)
  (make-path-annotation-primitive
   content
   label
   other-options
   shape
   color
   text-align
   text-width
   position
   on-slides))

;; (defstruct path
;;   from-node
;;   to-node
;;   (style "line")
;;   (arrow 'to)
;;   option-list
;;   annotation)

(define-record-type path
  (make-path-primitive
   from-node
   to-node
   style
   arrow
   option-list
   annotation)
  path?
  (from-node path-from-node)
  (to-node path-to-node)
  (style path-style)
  (arrow path-arrow)
  (option-list path-option-list)
  (annotation path-annotation))

(define* (make-path
          from-node
          to-node
          #:optional
          (style "line")
          (arrow 'to)
          option-list
          annotation)
  (make-path-primitive
   from-node
   to-node
   style
   arrow
   option-list
   annotation))

(define* (tikz-picture content #:optional show-grid-p)
  (tex-env "tikzpicture" content #f (if show-grid-p "show background grid" #f)))

(define (draw-node node)
  (let
      ((content (node-content node))
       (label (node-label node))
       (other-options (node-other-options node))
       (shape (node-shape node))
       (color (node-color node))
       (text-width (node-text-width node))
       (text-align (node-text-align node))
       (location (node-location node))
       (on-slides (node-on-slides node)))
    (string-append
     (tex-cmd 'node #f (string-append
                         "draw, "
                         "text width=" text-width ", "
                         (if other-options (string-append other-options ", ") "")
                         shape ", "
                         color ", "
                         text-width ", "
                         text-align))
     " ("
     label
     ") at ("
     (tex-to-string (point-x location))
     ","
     (tex-to-string (point-y location))
     ") "
     (wrap-braces
      (if on-slides
          (onslide (tex-to-string on-slides))
          #f)
      (wrap-braces content))
     ";")))

;; TODO with-slot-values (?)
(define (draw-path-annotation path-annotation)
  (let
      ((content (path-annotation-content path-annotation))
       (label (path-annotation-label path-annotation))
       (other-options (path-annotation-other-options path-annotation))
       (shape (path-annotation-shape path-annotation))
       (color (path-annotation-color path-annotation))
       (text-align (path-annotation-text-align path-annotation))
       (text-width (path-annotation-text-width path-annotation))
       (position (path-annotation-position path-annotation)))
    (string-append
     "node"
     (wrap-brackets-short (string-append
                           shape ", "
                           color ", "
                           text-align ", "
                           text-width ", "
                           (if other-options (string-append other-options ", ") #f)
                           position))
     (wrap-parens-short label)
     (wrap-braces content))))

(define (draw-path path)
  (let ((from-node (path-from-node path))
        (to-node (path-to-node path))
        (style (path-style path))
        (arrow (path-arrow path))
        (annotation (path-annotation path))
        (option-list (path-option-list path)))
    (string-append
     (tex-cmd 'path #f (string-append
                         "draw, shorten>=15pt"
                         (cond
                          ((equal? arrow 'to) "-latex'")
                          ((equal? arrow 'from) "latex'-")
                          ((equal? arrow 'both) "latex'-latex'"))))
     (wrap-parens-short from-node)
     " -- "
     (if annotation (draw-path-annotation annotation) #f)
     (wrap-parens-short to-node)
     ";")))
